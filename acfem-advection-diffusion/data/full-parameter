####
# IO Parameter
###

# process rank that prints output, -1 means disabled
fem.verboserank: 0
# fem Prefix
fem.prefix: .
# data output path and file prefix
fem.io.path: .
fem.io.datafileprefix: poisson

# Outputformat
# vtk-cell, vtk-vertex, sub-vtk-cell, sub-vtk-vertex, gnuplot
fem.io.outputformat: vtk-cell
fem.io.conforming: false
# time interval for data output
fem.io.savestep: 0
fem.io.savecount: 0

# 1d Grid File to be read in
fem.io.macroGridFile_1d: unitcube-1d.dgf
# 2d Grid File to be read in
fem.io.macroGridFile_2d: unitcube-2d.dgf
# 3d Grid File to be read in
fem.io.macroGridFile_3d: unitcube-3d.dgf

###
# Storage Parameter
###

# additional memory overload before compress of dofvector
fem.dofmanager.memoryfactor: 1.1
#
fem.solver.matrix.overflowfraction: 1


###
# Solver Parameter
###
# Solver Method
# gmres, cg, bicgstab, minres, gradient, loop, superlu
# comment to use automatic solver selection
# acfem.schemes.elliptic.solver.method: cg

# if true solver iterations are printed
acfem.schemes.elliptic.solver.verbose: false
# tolerance for the linear solver
acfem.schemes.elliptic.solver.tolerance: 1e-8

# error measure for the linear solver
# relative, absolute, residualreduction
acfem.schemes.elliptic.solver.errormeasure: absolute
# maximum number of iterations of the linear solver
acfem.schemes.elliptic.solver.maxiterations:100000

# as gmres is chosen, choose restart of gmres
acfem.schemes.elliptic.solver.gmres.restart: 20

# preconditioner
# none, ssor, ilu, sor, gauss-seidel, jacobi, amg-ilu, amg-jacobi, ildl
acfem.schemes.elliptic.solver.preconditioning.method: ilu
acfem.schemes.elliptic.solver.preconditioning.relaxation: 1.2
acfem.schemes.elliptic.solver.preconditioning.iterations: 2
acfem.schemes.elliptic.solver.preconditioning.fastilustorage: true

####
# Model Parameter
####

# tolerance of (small and local) newton scheme for possible non-linear dirichlet data
acfem.dirichlet.newton.tolerance: 1e-16
acfem.dirichlet.newton.iterationLimit: 100

####
# Adaptation Parameter
####

# initial number of uniform grid refinements
acfem.initialrefinements:2

# Estimator tolerance for adaptation
acfem.schemes.elliptic.adaptation.tolerance: 0.1

# Marking Strategy
acfem.schemes.elliptic.adaptation.markingStrategy: maximum
acfem.schemes.elliptic.adaptation.refineTolerance: 0
acfem.schemes.elliptic.adaptation.coarsenTolerance: 1.79769e+308
acfem.schemes.elliptic.adaptation.coarsenToleranceUniform: 0.05

# Fem adaptation method
# none, generic, callback
fem.adaptation.method: generic
# if parallel, number of adaptations until loadbalance
fem.loadbalancing.step: 1

