#include <config.h>

// iostream includes
#include <iostream>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune;
using namespace Dune::ACFem;

/**Implement an elliptic toy problem with somewhat complicated
 * boundary conditions in order to test the modular discrete-model
 * frame-work of Dune::ACFem.
 *
 * The "exact solution" @f$u(x) = e^{-C\,|x|^2}@f$ is used in order
 * to construct all data. The discretization will reproduce the
 * "solution" on the unit-square with consecutive boundary ids, like
 * defined by the following DGF-file:
 *
@code
DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

BoundaryDomain
default 3
1   0 0   1 0 % lower boundary
2   0 1   1 1 % upper boundary
#

#
@endcode
 *
 * The code imposes Dirichlet boundary condition on the left and right
 * boundaries (boundary id 3), homogeneous Neumann boundary conditions
 * on the lower boundary (boundary id 1) and Robin-boundary conditions
 *
 * @f[
 * \frac{\partial u}{\partial \nu} = C\,(-e^{-C\,|x|^2} - u)
 * @f]
 *
 * on the upper boundary (bounday id 2). The bulk-equations reads:
 *
 * \f[
 * -\Delta\,u + 4\,C^2\,|x|^2\,u = 2\,C\,d\,e^{-C\,|x|^2}
 * \f]
 *
 * After defining auxiliary variables the discrete model is finally constructed in symbolic notation
 *
 * @code
auto pdeModel = (-Delta_U + 4.0*(C*C)*sqr(X)*U - F
                 +
                 (Dbc0 - gD) + (Nbc0 - gN) + C*(Rbc0 - gR));
@endcode
 *
 * and then passed via an EllipticFemScheme to an adaptiveAlgorithm():
 *
 * @code
SchemeType scheme(solution, pdeModel, exactSolution);

return adaptiveAlgorithm(scheme);
@endcode
 *
 * The constant @f$C@f$ is chosen as .5 in this example in order to
 * have "numerically non-zero" normal derivatives at the boundary.
 *
 */
template<class HGridType>
std::pair<double, double> algorithm(HGridType &hGrid)
{
  auto discreteSpace = discreteFunctionSpace(hGrid, lagrange<POLORDER>);
  const auto& gridPart = discreteSpace.gridPart();
  auto solution = discreteFunction(discreteSpace, "solution");

  // Define some basic ingredients
  auto X = gridFunction(gridPart, [](auto&& x) { return x; });
  double k = Fem::Parameter::getValue<double>("reaction-diffusion.k",1);
  double sigma = Fem::Parameter::getValue<double>("reaction-diffusion.sigma",1);
  double C = 10.;
  auto dimDomain = intFraction<discreteSpace.dimDomain>();
  auto exactSolution = exp(-C*inner(X,X));
  exactSolution.setName("Exact Solution");
  auto F = k * 2 * C * (dimDomain - 2 * C * inner(X,X)) * exactSolution + sigma * pow(exactSolution,4);

  // homogeneous boundary models
  auto Dbc = dirichletBoundaryModel(exactSolution, EntireBoundaryIndicator());

  // bulk contributions
  auto DU_DPhi = laplacianModel(discreteSpace);
  auto U4 = p_MassModel(4, discreteSpace);

  // now define the discrete model ...
  auto pdeModel = k * DU_DPhi + sigma * U4 - F + Dbc;

  // create adaptive scheme, with exact solution for testing
  // purposes. The exact solution defaults to the ZeroGridFunction if
  // not specified. The scheme has to be passed as mutable reference
  // to the algorithm, hence the extra variable.
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);

  return adaptiveAlgorithm(scheme);
}

// main
// ----

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(DATADIR "/" "parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = DATADIR + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >("acfem.initialrefinements",2);

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ... quasi adapt_method_stat()
  std::pair<double,double> estimateError = algorithm(grid);

  std::cout << "Estimated Global Error: " << estimateError.first << std::endl;
  std::cout << "Real Global Error: " << estimateError.second << std::endl;

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
