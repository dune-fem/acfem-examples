#!/bin/bash 
cnt0=0	
initRefL=2 #initialRefinementLevel
for cnt in "0.2" "0.1" "0.05" "0.025" "0.0125" "0.00625" #"0.003125" "0.0015625" "0.00078125"
  do
    ./heat heat.timeStep:$cnt heat.initialRefinementLevel:$initRefL heat.adaptation.time.strategy:explicit heat.adaptation.space.markingStrategy:none heat.adaptation.initial.markingStrategy:none heat.adaptation.space.maximumIterations:0 heat.adaptation.initial.maximumIterations:0 > heatRun-TimeStep:$cnt.txt
    #grep Estimate\: heatRun-TimeStep:$cnt.txt | sed s/Estimate\:\ //g
    newError=$(grep Estimated\ Global\ Error\: heatRun-TimeStep:$cnt.txt | sed s/Estimated\ Global\ Error\:\ //g)

     echo -n "Error: "
     echo $newError
    
    if [ $cnt0 -gt 0 ]
    then
    echo -n "Computed EOC $cnt0: "
    echo "scale=20;l($oldError/$newError)/l(2)" | bc -l
    fi
    
    initRefL=$((initRefL+2)) # 2=refineStepsForHalf in 2D
    cnt0=$((cnt0+1))
    oldError=$newError
    
    rm heatRun-TimeStep:$cnt.txt
    
done

#read -p 'Press [Enter] key to remove Textfiles...'

#rm heatRun-TimeStep*.txt
