This project contains the examples for DUNE-ACFem. It requires the installation of dune-acfem (gitlab.dune-project.org/dune-fem/dune-acfem) and all dependencies, as described in the repository of DUNE-ACFem. The main examples solve a poisson and a heat equation, with dirichlet boundary conditions.


To compile the programs run the standard dune-control
```bash
./dune-common/bin/dunecontrol --opts=config.opts --module=acfem-poisson all
./dune-common/bin/dunecontrol --opts=config.opts --module=acfem-heat all
```

The configuration file `config.opts` has to contain the setting `-DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS:BOOL=TRUE` and `-DDUNE_GRID_GRIDTYPE_SELECTOR=ON`.
It might look as follows
```
# subdirectory to use as build-directory (this is the variable picked
# up by dunecontrol, not CMake!)
# BUILDDIR=test-shared

#Debugging flags
#STDFLAGS="-O0 -g3 -Wall -Wno-unused-local-typedefs -Wno-deprecated-declarations" # -Wno-unused-but-set-variable"
#Standard Flags -DDUNE_REENABLE_ADD_TEST:BOOL=TRUE\

STDFLAGS="-O3 -DNDEBUG -Wall -Wno-unused-local-typedefs -Wno-deprecated-declarations" # -Wno-unused-but-set-variable"

#Set C Compiler
CC=$(type -p gcc)
CXX=$(type -p g++)

# set flags for CMake:
# compiler flags, 3rd party software, grid extensions
#--debug-output --trace\
CMAKE_FLAGS="\
 -DCMAKE_C_COMPILER=${CC}\
 -DCMAKE_CXX_COMPILER=${CXX}\
 -DCMAKE_CXX_FLAGS='$STDFLAGS'\
 -DDUNE_GRID_GRIDTYPE_SELECTOR=ON\
"

MAKE_FLAGS=-j4
```

Once the programs are compiled the executables are located in `acfem-poisson/build-cmake/src/` and `acfem-heat/build-cmake/src/`.
The source code can be found in `acfem-poisson/src` and `acfem-heat/src`.
