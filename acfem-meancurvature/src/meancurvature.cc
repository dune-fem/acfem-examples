#include <config.h>

// iostream includes
#include <iostream>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune::ACFem;

/**Implement a constant mean curvature problem over the unit disc
 * (graph case) with zero Dirichlet boundary conditions. Consequently,
 * the solution should be a segment of a sphere (if the constant is
 * small enough). For constants smaller than 2 there is no solution to
 * this problem. NB, here "mean curvature" denotes the sum of the
 * principal curvature, not their algebraic mean value.
 *
@code
DGF

VERTEX
 0  0
-1  0
 0 -1
 1  0
 0  1
#

SIMPLEX
 1 2 0
 2 3 0
 3 4 0
 4 1 0
#

PROJECTION
function p(x) = x / |x|
default p
#

GRIDPARAMETER
NAME Projected-Disc
REFINEMENTEDGE LONGEST
#

#@endcode
 *
 */
template<class HGridType>
std::pair<double, double> algorithm(HGridType &hGrid)
{
  auto discreteSpace = discreteFunctionSpace(hGrid, lagrange<POLORDER>);
  const auto& gridPart = discreteSpace.gridPart();
  auto solution = discreteFunction(discreteSpace, "solution");

  // The mean curvature (sum of principle curavatures)
  const double H = Dune::Fem::Parameter::getValue<double>("constantMeanCurvature", 0.25);

  // Define some basic ingredients
  auto X = gridFunction(gridPart, [](auto&& x) { return x; });

  // Guess what ...
  const double R = 2_f / H;
  const double shift = sqrt(R*R - 1_f);
  auto exactSolution = sqrt(R*R - (sqr(X[0_c]) + sqr(X[1_c]))) - shift;

  // homogeneous boundary models
  auto Dbc = dirichletZeroModel(discreteSpace);

  // bulk contributions
  auto F = gridFunction(gridPart, [&](auto&& x) { return H; });

  auto MC = meanCurvatureModel(1_f, discreteSpace); // graph case

  // now define the discrete model ...
  auto pdeModel = MC - F + Dbc;

  // create adaptive scheme, with exact solution for testing
  // purposes. The exact solution defaults to the ZeroGridFunction if
  // not specified. The scheme has to be passed as mutable reference
  // to the algorithm.
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);

  return adaptiveAlgorithm(scheme);
}

// main
// ----

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(DATADIR "/" "parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = DATADIR + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue<int>("acfem.initialrefinements", 2);

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo<HGridType>::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ... quasi adapt_method_stat()
  std::pair<double,double> estimateError = algorithm(grid);

  std::cout << "Estimated Global Error: " << estimateError.first << std::endl;
  std::cout << "Real Global Error: " << estimateError.second << std::endl;

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
