# set up project
project("acfem-advection-diffusion" CXX)

#circumvent not building docs
set(BUILD_DOCS 1)

# general stuff
cmake_minimum_required(VERSION 2.8.6)

#find dune-common and set the module path
find_package(dune-common)
list(APPEND CMAKE_MODULE_PATH ${dune-common_MODULE_PATH}
     "${CMAKE_SOURCE_DIR}/cmake/modules")# make sure our own modules are found

#include the dune macros
include(DuneMacros)

# find and include special ACFem things
find_package(dune-acfem)
list(APPEND CMAKE_MODULE_PATH ${dune-acfem_MODULE_PATH}
     "${CMAKE_SOURCE_DIR}/cmake/modules")

# start a dune project with information from dune.module
dune_project()

# how's about just a specific package?
dune_enable_all_packages()

set(SUBDIRS src data)

if( BUILD_DOCS)
list(APPEND SUBDIRS doc)
endif( BUILD_DOCS )

foreach(i ${SUBDIRS})
  if(${i} STREQUAL "test")
    set(opt EXCLUDE_FROM_ALL)
  endif(${i} STREQUAL "test")
  add_subdirectory(${i} ${opt})
  unset(opt)
endforeach(i ${SUBDIRS})

add_subdirectory(cmake/modules)

dune_acfem_core_fixes()

# finalize the dune project, e.g., generate config.h etc.
finalize_dune_project(GENERATE_CONFIG_H_CMAKE)
