# process rank that prints output, -1 means disabled
fem.verboserank: 0

# if true solver iterations are printed
fem.solver.verbose: false

# preconditioning method:
# none, ssor, sor, ilu-0, ilu-n, gauss-seidel, jacobi, amg-ilu-n, amg-ilu-0, amg-jacobi
istl.preconditioning.method: none
istl.preconditioning.iterations: 2
istl.preconditioning.relaxation: 1.2

# preconditioning with diagonal preconditioner
fem.preconditioning: true

# 1D
fem.io.macroGridFile_1d: ../data/unitcube-1d.dgf

# 2D
fem.io.macroGridFile_2d: ../data/unitcube-2d.dgf
#fem.io.macroGridFile_2d: ../data/unstructured-2d.dgf
#fem.io.macroGridFile_2d: ../data/corner.dgf
#fem.io.macroGridFile_2d: ../data/examplegrid2b.dgf
#fem.io.macroGridFile_2d: ../data/corner_triangle.dgf

# 3D
fem.io.macroGridFile_3d: ../data/unitcube-3d.dgf

# data output path
fem.io.path: ../output
fem.io.datafileprefix: heat
fem.io.checkpointfile: checkpoints

# vtk-cell, vtk-vertex, sub-vtk-cell, sub-vtk-vertex, gnuplot
fem.io.outputformat: vtk-cell

# time interval for data output
fem.io.savestep: 0.1
fem.io.savecount: 10

# write a checkpoint after some steps ...
fem.io.checkpointstep: 2
fem.io.checkpointmax: 2

# CFL condition
fem.timeprovider.factor: 1

###############################################################################
#
# HEAT EQUATION
#

# restart from checkpoint or not.
heat.restart: false

# "constant", "spaceConstant", "timeConstant", "cosines"
heat.problem: cosines

# initial level of refinement
heat.initialRefinementLevel: 0

# start time
heat.startTime: 0.0

# end time
heat.endTime: 2.0

# Initial time step size. The time-step size remains fixed for the
# "explicit" time "adaptation" strategy and is controlled by a
# residual error estimator for the "implicit" time adaptation
# strategy.
heat.timeStep: 1e-2

# theta for the Theta-scheme. The error-estimator, however, only works
# well for the implicit Euler's method 'cause the temporal error is
# over-estmitated for the Crank-Nicholson method. Still it should be
# possible to use space-adaptivity to some extend (choosing the
# "explicit" time strategy with a fixed time-step size
heat.theta: 1.0

#
#
###############################################################################
#
# controls for adaptive method

heat.restart:

# the over-all tolerance
heat.adaptation.tolerance: 1e-1

# relative tolerance fraction relative to prescribed total tolerance
heat.adaptation.initialToleranceFraction: 0.2
heat.adaptation.spaceToleranceFraction:   0.4
heat.adaptation.timeToleranceFraction:    0.4

# time step control, "implicit" or "explicit"
heat.adaptation.time.strategy: implicit
heat.adaptation.time.narrowFactor: 0.7071
heat.adaptation.time.relaxFactor: 1.4142
heat.adaptation.time.relaxToleranceFraction: 0.3


# space strategies maximum, equitolerance, equidistribution, uniform, none
heat.adaptation.space.markingStrategy: equitolerance
heat.adaptation.space.maximumIterations: 20
heat.adaptation.space.refineTolerance:  1.3
heat.adaptation.space.coarsenTolerance: 0.7
heat.adaptation.space.coarsenToleranceUniform: 0.05
heat.adaptation.initial.markingStrategy: equitolerance
heat.adaptation.initial.maximumIterations: 20
heat.adaptation.initial.refineTolerance: 0.9
heat.adaptation.initial.coarsenTolerance: 0.4
heat.adaptation.initial.coarsenToleranceUniform: 0.05


# tolerance for the linear solver
heat.solvereps: 1e-10

#
#
###############################################################################

# adaptation method: none, generic, callback
fem.adaptation.method: generic
