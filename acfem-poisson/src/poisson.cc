#include <config.h>

#include <iostream>

// include acfem headers
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune;
using namespace Dune::ACFem;

// choose the finite element spaces
// pose the problem
// construct the scheme
// run the adaptive algorithm
template<class HGridType>
void algorithm(HGridType &hGrid)
{
  auto discreteSpace = discreteFunctionSpace(hGrid, lagrange<POLORDER>);
  const auto& gridPart = discreteSpace.gridPart();
  auto solution = discreteFunction(discreteSpace, "solution");

  // define manufactured solution
  auto X = gridFunction(gridPart, [](auto&& x) { return x; });
  auto C = 10;
  auto exactSolution = exp(-C * inner(X,X));
  exactSolution.setName("exact solution");

  // Dirichlet boundary conditions
  auto gD = exactSolution;
  auto Dbc = dirichletBoundaryModel(gD, BoundaryIdIndicator(1));

  // Robin boundary condition

  // fetch parameter acfem.robinfactor from parameter file and make it positive
  auto robinAlpha = positiveParameter<0 /* some id */>("acfem.robinfactor", 10.0);

  // Compute the proper RHS for the Robin-BC and exactSolution
  auto gR = (1 - 2 * C / robinAlpha * X[0_c]) * exactSolution;
  auto Rbc = robinBoundaryModel(gR, BoundaryIdIndicator(2));

  // bulk contributions
  auto DU_DPhi = laplacianModel(discreteSpace);

  // dimDomain is an enum, so convert it to an integral type
  constexpr int dD = discreteSpace.dimDomain;
  auto F = 2 * C * (dD - 2 * C * inner(X,X)) * exactSolution;

  // now define the discrete model ...
  auto pdeModel = DU_DPhi - F + Dbc + robinAlpha * Rbc;

  // create adaptive scheme, with exact solution for testing
  // purposes. The exact solution defaults to the ZeroGridFunction if
  // not specified.
  //auto scheme = ellipticFemScheme(solution, assume<PositiveExpression>(pdeModel), exactSolution);
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);

  // SOLVE - ESTIMATE - MARK - ADAPT - LOADBALANCE
  auto estimateError = adaptiveAlgorithm(scheme);

  if (Dune::Fem::MPIManager::rank() == 0) {
    std::cout << "Estimated Global Error: " << estimateError.first << std::endl;
    std::cout << "Real Global Error: " << estimateError.second << std::endl;
  }
}

// main
// ----

int main (int argc, char **argv)
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[i]);

  // append default parameter file
  Dune::Fem::Parameter::append(DATADIR "/" "parameter");

  // type of hierarchical grid
  using HGridType = Dune::GridSelector::GridType;

  // create grid from DGF file
  auto gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  auto gridfile = DATADIR + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // construct macro using the DGF Parser
  Dune::GridPtr<HGridType> gridPtr(gridfile);
  HGridType& grid = *gridPtr;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  auto level = Dune::Fem::Parameter::getValue<int>("acfem.initialrefinements", 2);

  // number of global refinements to bisect grid width
  auto refineStepsForHalf = Dune::DGFGridInfo<HGridType>::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ...
  algorithm(grid);

  return EXIT_SUCCESS;
}
