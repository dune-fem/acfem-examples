
#include <config.h>

// iostream includes
#include <iostream>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune::ACFem;

/**Implement an elliptic toy problem with somewhat complicated
 * boundary conditions in order to test the modular discrete-model
 * frame-work of Dune::ACFem.
 *
 * The "exact solution" @f$u(x) = e^{-C\,|x|^2}@f$ is used in order to
 * construct all data. The discretization will reproduce the
 * "solution" on the unit-square with boundary ids like defined by the
 * following DGF-file:
 *
@code
DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

BoundaryDomain
default 3
1   0 0   1 0 % lower boundary
2   0 1   1 1 % upper boundary
#

#
@endcode
 *
 * The code imposes Dirichlet boundary condition on the left and right
 * boundaries (boundary id 1 and 3), homogeneous boundary conditions
 * on the lower boundary and Robin-boundary conditions
 *
 * @f[
 * \frac{\partial u}{\partial \nu} = C\,(-e^{-C\,|x|^2} - u)
 * @f]
 *
 * on the lower boundary. The bulk-equation reads:
 *
 * \f[
 * -\Delta\,u + 4\,C^2\,e^{C\,|x|^2}\,|x|^2\,|u|^2 = 20\,d\,e^{-C\,|x|^2}
 * \f]
 *
 * After defining auxiliary variables the discrete model is finally constructed in symbolic notation
 *
 * @code
auto pdeModel = -Delta_U + 4.0*(C*C)*sqr(X)*exp(C*sqr(X))*U_U - F + Dbc + Nbc + C*Rbc;
@endcode
 *
 * and then passed via an EllipticFemScheme to an adaptiveAlgorithm():
 *
 * @code
SchemeType scheme(solution, pdeModel, exactSolution);

return adaptiveAlgorithm(scheme);
@endcode
 */
template<class HGridType>
std::pair<double, double> algorithm(HGridType &hGrid)
{
  auto discreteSpace = discreteFunctionSpace(hGrid, lagrange<POLORDER>);
  const auto& gridPart = discreteSpace.gridPart();
  auto solution = discreteFunction(discreteSpace, "solution");

  auto dimDomain = intFraction<discreteSpace.dimDomain>();

  // Define some basic ingredients
  auto X = gridFunction(gridPart, [](auto&& x) { return x; });
  auto C = 10_f;

  auto exactSolution = exp(-C*inner(X, X));

  // Boundary indicators: Neumann on Id 1, Robin on Id 2, else Dirichlet
  auto gR = -exp(-C*(sqr(X[0_c])+1_f));
  auto gD = exactSolution;

  // homogeneous boundary models
  auto Dbc = dirichletBoundaryModel(gD, !(BoundaryIdIndicator(1) || BoundaryIdIndicator(2)));
  auto Rbc = robinBoundaryModel(gR, BoundaryIdIndicator(2));

  // bulk contributions
  auto F = 2_f * C * dimDomain * exactSolution;
  auto DU_DU = laplacianModel(discreteSpace);
  auto U_U = p_MassModel(3_f, discreteSpace); // 3.0 _IS_ correct

  // now define the discrete model ...
  auto pdeModel = DU_DU + 4_f*(C*C)*inner(X, X)*exp(C*inner(X, X))*U_U - F + Dbc + C*Rbc;

  // create adaptive scheme, with exact solution for testing
  // purposes. The exact solution defaults to the ZeroGridFunction if
  // not specified. The scheme has to be passed as mutable reference
  // to the algorithm
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);

  return adaptiveAlgorithm(scheme);
}

// main
// ----

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(DATADIR "/" "parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = DATADIR + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >("acfem.initialrefinements",2);

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ... quasi adapt_method_stat()
  std::pair<double,double> estimateError = algorithm(grid);

  std::cout << "Estimated Global Error: " << estimateError.first << std::endl;
  std::cout << "Real      Global Error: " << estimateError.second << std::endl;

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
