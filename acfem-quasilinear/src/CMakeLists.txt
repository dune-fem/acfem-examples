# The name of the program to be generated
set(PROGRAM quasilinear)

#set(GRIDTYPE ALBERTAGRID CACHE STRING "Gridtype to use")
set(GRIDTYPE ALUGRID_CONFORM CACHE STRING "Gridtype to use")
set(GRIDDIM  2 CACHE STRING "Grid-dimension to use")
set(POLORDER  2 CACHE STRING "Polynomial order to use")
set(DATADIR ${CMAKE_SOURCE_DIR}/data/ CACHE STRING "Location of parameter and DGF files")

set(WANT_ISTL 1 CACHE STRING "Set to 1 to use ISTL")
set(WANT_PETSC 0 CACHE STRING "Set to 1 to use PETSC")

add_definitions(
 -D${GRIDTYPE}
 -DPOLORDER=${POLORDER}
 -DGRIDDIM=${GRIDDIM}
 -DWANT_ISTL=${WANT_ISTL}
 -DWANT_PETSC=${WANT_PETSC}
 -DDATADIR=\"${DATADIR}\"
)

add_executable(${PROGRAM} quasilinear.cc)

if(${GRIDTYPE} STREQUAL ALBERTAGRID)
  add_dune_alberta_flags(${PROGRAM} WORLDDIM ${GRIDDIM})
endif()

add_dune_acfem_papi_flags(${PROGRAM})

add_custom_command(TARGET ${PROGRAM} PRE_BUILD
  COMMAND ${CMAKE_COMMAND} -E make_directory $<TARGET_FILE_DIR:${PROGRAM}>/../output)

#add_custom_command(TARGET quasilinear PRE_BUILD
#  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/data/ $<TARGET_FILE_DIR:quasilinear>/../data/)
